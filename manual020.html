<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
<meta name="generator" content="hevea 2.36">
<meta name="Author" content="Luc Maranget">
<script type="text/javascript" async src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.1/MathJax.js?config=TeX-MML-AM_CHTML"></script><link rel="stylesheet" type="text/css" href="manual.css">
<title>Customising HEVEA</title>
</head>
<body>
<a href="manual019.html"><img src="previous_motif.svg" alt="Previous"></a>
<a href="manual002.html"><img src="contents_motif.svg" alt="Up"></a>
<a href="manual021.html"><img src="next_motif.svg" alt="Next"></a>
<hr>
<h2 class="section" id="sec89">10&#X2003;Customising H<span class="c020"><sup>E</sup></span>V<span class="c020"><sup>E</sup></span>A</h2>
<ul>
<li><a href="manual020.html#sec90">Simple changes</a>
</li><li><a href="manual020.html#sec91">Changing defaults for type-styles</a>
</li><li><a href="manual020.html#sec92">Changing the interface of a command</a>
</li><li><a href="manual020.html#sec93">Checking the optional argument within a command</a>
</li><li><a href="manual020.html#sec94">Changing the format of images</a>
</li><li><a href="manual020.html#sec95">Storing images in a separate directory</a>
</li><li><a href="manual020.html#imagen-source">Controlling <span class="c017">imagen</span> from document source</a>
</li></ul>
<p>
H<span class="c020"><sup>E</sup></span>V<span class="c020"><sup>E</sup></span>A can be controlled by writing L<sup>A</sup>T<sub>E</sub>X code. In this section,
we examine how users can change H<span class="c020"><sup>E</sup></span>V<span class="c020"><sup>E</sup></span>A default behaviour or add
functionalities. In all this section we assume that a document
<span class="c017">doc.tex</span> is processed, using a private command file
<span class="c017">macros.hva</span>. That is, H<span class="c020"><sup>E</sup></span>V<span class="c020"><sup>E</sup></span>A is invoked as:
</p><pre class="verbatim"># hevea macros.hva doc.tex
</pre><p>
The general idea is as follows: one redefines L<sup>A</sup>T<sub>E</sub>X constructs in
<span class="c017">macros.hva</span>, using internal commands. This requires a good
working knowledge of both L<sup>A</sup>T<sub>E</sub>X and html.
Usually, one can avoid internal commands, but then, all command
redefinitions interact, sometimes in very nasty ways.</p>
<h3 class="subsection" id="sec90">10.1&#X2003;Simple changes</h3>
<p>
Users can easily change the rendering of some constructs. For
instance, assume that <em>all</em> quotations in a text should be
emphasised. Then, it suffices to put the following re-declaration in
<span class="c017">macros.hva</span>:
</p><pre class="verbatim">\renewenvironment{quote}
  {\@open{blockquote}{}\@style{em}}
  {\@close{blockquote}}
</pre><p>The same effect can be achieved without using any of the internal
commands:
</p><pre class="verbatim">\let\oldquote\quote
\let\oldendquote\endquote
\renewenvironment{quote}{\oldquote\em}{\oldendquote}
</pre><p>
In some sense, this second 
solution is easier, when one already knows
how to customise L<sup>A</sup>T<sub>E</sub>X. However, this is less safe, since the definition of
<code class="verb">\em</code> can be changed elsewhere.</p><p>There is yet another solution that takes advantage of style sheets.
One can also add this line to the <span class="c017">macros.hva</span> file:
</p><pre class="verbatim">\newstyle{.quote}{font-style:oblique;}
</pre><p>
This works because the environment <span class="c017">quote</span> is styled through
style class <span class="c017">quote</span> (see Section&#XA0;<a href="manual019.html#css%3Achange%3Aall">9.2</a>).
Notice that this solution has very little to do with
&#X201C;<em>emphasising</em>&#X201D; in the proper sense, since here we
short-circuit the implicit path from <code class="verb">\em</code> to oblique fonts.</p>
<h3 class="subsection" id="sec91">10.2&#X2003;Changing defaults for type-styles</h3>
<p><a id="customize-style"></a>
H<span class="c020"><sup>E</sup></span>V<span class="c020"><sup>E</sup></span>A default rendering of type style changes is described in
section&#XA0;<a href="manual037.html#type-style">B.15.1</a>.
For instance, the following example shows the default rendering
for the font shapes:
</p><pre class="verbatim">\itshape italic shape \slshape slanted shape
\scshape small caps shape \upshape upright shape
</pre><p>
By default, <code class="verb">\itshape</code> is italics, <code class="verb">\slshape</code> is oblique
italics, <code class="verb">\scshape</code> is small-caps (thanks to style sheets) and <code class="verb">\upshape</code> is no style at all.
All shapes are mutually exclusive, this means that each shape
declaration cancels the effect of other active shape declarations.
For instance, in the example, small caps shapes is small caps (no italics here).

</p><blockquote class="quote">
<span class="c023">italic shape </span><span class="c024">slanted shape
</span><span class="c025">small caps shape </span>upright shape
</blockquote><p>If one wishes to change the rendering of some of the shapes (say slanted
caps), then one should redefine the old-style <code class="verb">\sl</code> declaration.
For instance, to render slanted as Helvetica (why so?), one should
redefine <code class="verb">\sl</code> by <code class="verb">\renewcommand{\sl}{\@span{style="font-family:Helvetica"}}</code> in
<span class="c017">macros.hva</span>.</p><p>
And now, the shape example above gets rendered as follows:
</p><blockquote class="quote">
<span class="c023">italic shape </span><span class="c016">slanted shape
</span><span class="c025">small caps shape </span>upright shape
</blockquote><p>Redefining the old-style <code class="verb">\sl</code> is compatible with the cancellation
mechanism, redefining <code class="verb">\slshape</code> is not.
Thus, redefining directly L<sup>A</sup>T<sub>E</sub>X&#XA0;2&#X454; <code class="verb">\slshape</code> with
<code class="verb">\renewcommand{\slshape}{}</code> would yield:
</p><blockquote class="quote">
<span class="c023">italic shape <span class="c016">slanted shape
</span></span><span class="c016"><span class="c025">small caps shape </span>upright shape</span>
</blockquote><p>Hence, redefining old-style declarations using internal commands
should yield satisfactory output.
However, since cancellation is done at the html
level, a declaration belonging to one component may sometimes cancel the
effect of another that belongs to another component.
Anyway, you might have not noticed it if I had not told you.</p>
<h3 class="subsection" id="sec92">10.3&#X2003;Changing the interface of a command</h3>
<p><a id="customize-let"></a>
Assume for instance that the base style of <span class="c017">doc.tex</span> is
<span class="c023">jsc</span> (the
<em>Journal of Symbolic Computation</em> style for articles).
For running H<span class="c020"><sup>E</sup></span>V<span class="c020"><sup>E</sup></span>A, the <span class="c023">jsc</span> style can be replaced by
<span class="c023">article</span>
style, but for a few commands whose calling interface is changed.
In particular, the <code class="verb">\title</code> command
takes an extra optional argument (which H<span class="c020"><sup>E</sup></span>V<span class="c020"><sup>E</sup></span>A should ignore
anyway).
However, H<span class="c020"><sup>E</sup></span>V<span class="c020"><sup>E</sup></span>A can process the document as it stands.
One solution to insert the following lines into <span class="c017">macros.hva</span>:
</p><pre class="verbatim">\input{article.hva}% Force document class 'article'
\let\oldtitle=\title
\renewcommand{\title}[2][]{\oldtitle{#2}}
</pre><p>
The effect is to replace <code class="verb">\title</code> by a new command which
calls H<span class="c020"><sup>E</sup></span>V<span class="c020"><sup>E</sup></span>A <code class="verb">\title</code> with the appropriate argument.
</p>
<h3 class="subsection" id="sec93">10.4&#X2003;Checking the optional argument within a command</h3>
<p><a id="fullepsfbox"></a>
<a id="hevea_default132"></a>
H<span class="c020"><sup>E</sup></span>V<span class="c020"><sup>E</sup></span>A fully implements L<sup>A</sup>T<sub>E</sub>X&#XA0;2&#X454; <code class="verb">\newcommand</code>.
That is, users can define commands with an optional argument.
Such a feature permits to write a <code class="verb">\epsfbox</code> command that
has the same interface as the L<sup>A</sup>T<sub>E</sub>X command and
echoes itself as it is invoked to the <span class="c023">image</span> file.
To do this, the H<span class="c020"><sup>E</sup></span>V<span class="c020"><sup>E</sup></span>A <code class="verb">\epsfbox</code> command has to check
whether it is invoked with an optional argument or not.
This can be achieved as follows:
</p><pre class="verbatim">\newcommand{\epsfbox}[2][!*!]{%
\ifthenelse{\equal{#1}{!*!}}
{\begin{toimage}\epsfbox{#2}\end{toimage}}%No optional argument
{\begin{toimage}\epsfbox[#1]{#2}\end{toimage}}}%With optional argument
\imageflush}
</pre>
<h3 class="subsection" id="sec94">10.5&#X2003;Changing the format of images</h3>
<p>
<a id="hevea_default133"></a>
<a id="hevea_default134"></a><a id="hevea_default135"></a><a id="hevea_default136"></a>
<a id="hevea_default137"></a>
<a id="hevea_default138"></a>
Semi-automatic generation of included images is described in
section&#XA0;<a href="manual008.html#imagen">6</a>.
Links to included images are generated by the <code class="verb">\imageflush</code>
command, which calls the <code class="verb">\imgsrc</code> command:
</p><pre class="verbatim">\newcommand{\imageflush}[1][]
{\@imageflush\stepcounter{image}\imgsrc[#1]{\hevaimagedir\jobname\theimage\heveaimageext}}
</pre><p>
That is, you may supply a html-style attribute to the included image,
as an optional argument to the <code class="verb">\imageflush</code> command.</p><p>By default, images are PNG images stored in <span class="c017">.png</span> files.
H<span class="c020"><sup>E</sup></span>V<span class="c020"><sup>E</sup></span>A provides support for the alternative GIF image file format.
It suffices to invoke <span class="c017">hevea</span> as:
</p><div class="flushleft">
<span class="c017">#&#XA0;hevea&#XA0;gif.hva</span>&#XA0;<span class="c023">doc.tex</span>
</div><p>
Then <span class="c017">imagen</span> must be run with option <a id="hevea_default139"></a><span class="c017">-gif</span>:
</p><div class="flushleft">
<span class="c017">#&#XA0;imagen&#XA0;-gif</span>&#XA0;<em>doc</em>
</div><p>A convenient alternative is to invoke <span class="c017">hevea</span> as:
</p><div class="flushleft">
<span class="c017">#&#XA0;hevea&#XA0;-fix&#XA0;gif.hva</span>&#XA0;<span class="c023">doc.tex</span>
</div><p>
Then <span class="c017">hevea</span> will invoke <span class="c017">imagen</span> with the appropriate
option when it thinks images need to be rebuild.
An even more convenient alternative is to load <span class="c017">gif.hva</span>
from within document source, for instance with the <code class="verb">\usepackage</code>
command.</p><p>H<span class="c020"><sup>E</sup></span>V<span class="c020"><sup>E</sup></span>A also provides support for the alternative SVG image file format.
As for GIF images, it is more convenient to use option <span class="c017">-fix</span>
to combine <span class="c017">hevea</span> and <span class="c017">imagen</span> invocations:
</p><div class="flushleft">
<span class="c017">#&#XA0;hevea&#XA0;-fix&#XA0;svg.hva</span>&#XA0;<span class="c023">doc.tex</span>
</div><p>
Notice that <span class="c017">imagen</span> production chain of SVG images always
call <span class="c017">pdflatex</span>, even when <em>not</em> given
the <a href="manual041.html#imagenoptions"><span class="c017">-pdf</span> command-line option</a>.
Hence the source code of images must be processable
by <span class="c017">pdflatex</span>. This precludes using <span class="c017">latex</span>-only packages
such as pstricks for instance.</p><p>As not all browsers display SVG images, <span class="c017">hevea</span> and
<span class="c017">imagen</span> are bit special:
<span class="c017">imagen</span> produces both PNG<sup><a id="text10" href="#note10">9</a></sup> and SVG&#XA0;images; while <span class="c017">hevea</span> offers both image sources,
letting client browser select the most appropriate one by the means of
the <code class="verb">srcset</code> attribute of the <code class="verb">img</code> element.</p>
<h3 class="subsection" id="sec95">10.6&#X2003;Storing images in a separate directory</h3>
<p> 
<a id="hevea_default140"></a>
By redefining the <code class="verb">\heveaimagedir</code> command, users can specify a
directory for images.
More precisely, if the following redefinition occurs in the document
preamble.
</p><div class="flushleft">
<code class="verb">\renewcommand{\heveaimagedir}{</code><span class="c023">dir</span><code class="verb">}</code>
</div><p>
Then, all links to images in the produced html file will be as
&#X201C;<span class="c023">dir</span>/&#X2026;&#X201D;.
Then <span class="c017">imagen</span> must be invoked with option&#XA0;<a id="hevea_default141"></a><span class="c017">-
todir</span>:
</p><div class="flushleft">
<span class="c017">#&#XA0;imagen&#XA0;-todir</span>&#XA0;<span class="c023">dir</span>&#XA0;<em>doc</em>
</div><p>
As usual, <span class="c017">hevea</span> will invoke <span class="c017">imagen</span> with the
appropriate option, provided it is passed the <span class="c017">-fix</span> option.</p>
<h3 class="subsection" id="imagen-source">10.7&#X2003;Controlling <span class="c017">imagen</span> from document source</h3>
<p>
<a id="hevea_default142"></a>
The internal command
<code class="verb">\@addimagenopt{</code><span class="c023">option</span><code class="verb">}</code> add
the text <span class="c023">option</span> to <span class="c017">imagen</span> command-line options, when
launched automatically by <span class="c017">hevea</span> (<em>i.e.</em> when
<span class="c017">hevea</span> is given the <a id="hevea_default143"></a><span class="c017">-fix</span> command-line option).</p><p>For instance, to instruct <span class="c017">hevea</span>/<span class="c017">imagen</span> to
reduce all images by a factor of &#X221A;<span style="text-decoration:overline">2</span>, it suffices to state:
</p><div class="flushleft">
<span class="c017">%HEVEA</span><code class="verb">\@addimagenopt{-mag 707}</code>
</div><p>
See section&#XA0;<a href="manual041.html#imagenusage">C.1.5</a> for the list of command-line options
accepted by <span class="c017">imagen</span>.</p>
<hr class="ffootnoterule"><dl class="thefootnotes"><dt class="dt-thefootnotes"><a id="note10" href="#text10">9</a></dt><dd class="dd-thefootnotes"><div class="footnotetext">or GIF, if <span class="c017">gif.hva</span> is loaded</div></dd></dl>
<hr>
<a href="manual019.html"><img src="previous_motif.svg" alt="Previous"></a>
<a href="manual002.html"><img src="contents_motif.svg" alt="Up"></a>
<a href="manual021.html"><img src="next_motif.svg" alt="Next"></a>
</body>
</html>
